# Python3爬虫入门到精通课程视频

#### 介绍
Python3爬虫入门到精通课程视频
链接：https://pan.baidu.com/s/1gTZUptVGd4Ruu6pNEHxjsw 
提取码：8c2x 


#### 目录

├─章节1： 环境配置    
│      课时01：Python3+Pip环境配置.mp4    
│      课时02：MongoDB环境配置.mp4    
│      课时03：Redis环境配置.mp4    
│      课时04：MySQL的安装.mp4    
│      课时05：Python多版本共存配置.mp4    
│      课时06：Python爬虫常用库的安装.mp4    
│      
├─章节2： 基础篇    
│      课时07：爬虫基本原理讲解.mp4    
│      课时08：Urllib库基本使用.mp4    
│      课时09：Requests库基本使用.mp4    
│      课时10：正则表达式基础.mp4    
│      课时11：BeautifulSoup库详解.mp4    
│      课时12：PyQuery详解.mp4    
│      课时13：Selenium详解.mp4    
│      
├─章节3： 实战篇    
│      课时14：Requests+正则表达式爬取猫眼电影.mp4    
│      课时15：分析Ajax请求并抓取今日头条街拍美图 .mp4    
│      课时16：使用Selenium模拟浏览器抓取淘宝商品美食信息.mp4    
│      课时17：使用Redis+Flask维护动态代理池.mp4    
│      课时18：使用代理处理反爬抓取微信文章.mp4    
│      课时19：使用Redis+Flask维护动态Cookies池.mp4    
│      
├─章节4： 框架篇    
│      课时20：PySpider框架基本使用及抓取TripAdvisor实战.mp4    
│      课时21：PySpider架构概述及用法详解.mp4    
│      课时22：Scrapy框架安装.mp4    
│      课时23：Scrapy框架基本使用.mp4    
│      课时24：Scrapy命令行详解.mp4    
│      课时25：Scrapy中选择器用法.mp4    
│      课时26：Scrapy中Spiders用法.mp4    
│      课时27：Scrapy中Item Pipeline的用法.mp4    
│      课时28：Scrapy中Download Middleware的用法.mp4    
│      课时29：Scrapy爬取知乎用户信息实战.mp4    
│      课时30：Scrapy+Cookies池抓取新浪微博.mp4    
│      课时31：Scrapy+Tushare爬取微博股票数据.mp4    
│      
└─章节5： 分布式篇    
            课时32：Scrapy分布式原理及Scrapy-Redis源码解析.mp4    
            课时33：Scrapy分布式架构搭建抓取知乎.mp4    
            课时34：Scrapy分布式的部署详解.mp4    

